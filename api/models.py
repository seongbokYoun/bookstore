import uuid
from django.db import models

class Book(models.Model):
    isbn = models.CharField(primary_key=True, max_length=200)
    payment_type = models.CharField(max_length=20)
    price = models.IntegerField()

    def __str__(self):
        return self.isbn

class User(models.Model):
    id = models.CharField(primary_key=True, max_length=30)
    own_books = models.ManyToManyField(Book, symmetrical=False)
    balance = models.IntegerField(default=0)

    def __str__(self):
        return self.id