from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('user_books', views.user_books, name='user_books'),
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('buy_book', views.buy_book, name='buy_book'),
]