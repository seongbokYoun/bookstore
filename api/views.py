from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .models import Book, User

@login_required
def user_books(request):
    user = get_object_or_404(User, id=request.user.username)
    books = user.own_books.all()
    
    response = {}
    for book in books:
        response_book_list = response.get(book.payment_type, None)
        if response_book_list is None:
            response_book_list = list()
            response[book.payment_type] = response_book_list
        response_book_list.append(book.isbn)
    
    return JsonResponse(response, json_dumps_params={'indent': 2})

def jsonCustomResponse(message, status=200):
    response = {}
    response['message'] = message
    return JsonResponse(response, json_dumps_params={'indent': 2}, status=status)

@login_required
def buy_book(request):
    if request.method == 'POST':

        #TODO input validation
        isbn = request.POST['isbn']
        book = get_object_or_404(Book, isbn=isbn)
        
        user = get_object_or_404(User, id=request.user.username)
        if user.balance < book.price:
            return jsonCustomResponse('Not enough balance', 400)

        if user.own_books.filter(isbn=isbn).count() > 0:
            return jsonCustomResponse('Already having book', 400)

        user.balance -= book.price
        user.own_books.add(book)
        user.save()

        return jsonCustomResponse('success')
    else:
        return render(request, 'buy.html', {
            'Books': Book.objects.all()
        })
    return None