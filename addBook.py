from api.models import *
import random

def insertBooks():
    for i in range(0, 100):
        if random.random() > 0.9:
            pt = 'paid'
            pr = random.randrange(1000,50000)
        else:
            pt = 'free'
            pr = 0
        book = Book(isbn="isbn-{}".format(i), payment_type=pt, price=pr)
        book.save()